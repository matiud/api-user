package com.Nisum.apiUser.service;


import com.Nisum.apiUser.exception.DuplicateUserException;
import com.Nisum.apiUser.exception.InvalidDataException;
import com.Nisum.apiUser.exception.InvalidLoginException;
import com.Nisum.apiUser.model.dto.SuccessfulRegistrationResponseDTO;
import com.Nisum.apiUser.model.dto.UserLoginDTO;
import com.Nisum.apiUser.model.dto.UserRegistrationDTO;
import com.Nisum.apiUser.model.entity.User;
import com.Nisum.apiUser.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UserService {
     @Autowired
     private UserRepository userRepository;
    @Value("${jwt.secretKey}")
    private String secretKey;
    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();


    public String loginUser(UserLoginDTO userLoginDTO) {
        // Realiza la autenticación del usuario utilizando el correo y la contraseña proporcionados.
        User user = userRepository.findByEmail(userLoginDTO.getEmail())
                .orElseThrow(() -> new InvalidLoginException("Invalid login credentials"));

        if (passwordMatches(userLoginDTO.getPassword(), user.getPassword())) {
            // Las credenciales son correctas, genera un token de acceso y devuélvelo.
            String token = generateAccessToken(user.getEmail());
            return token;
        } else {
            throw new InvalidLoginException("Invalid login credentials");
        }
    }

    // Implementa la lógica para verificar si la contraseña coincide con la almacenada en la base de datos.
    private boolean passwordMatches(String inputPassword, String storedPassword) {
        return passwordEncoder.matches(inputPassword, storedPassword);
    }

    // Implementa la lógica para generar un token de acceso.
    private String generateAccessToken(String userEmail) {
        // Define las claims que deseas incluir en el token (por ejemplo, el nombre de usuario)
        Claims claims = Jwts.claims().setSubject(userEmail);

        // Define la fecha de expiración del token (puedes personalizar esto)
        Date expirationDate = new Date(System.currentTimeMillis() + 3600000); // 1 hora de duración

        // Genera el token utilizando la clave secreta y algoritmo de firma
        String token = Jwts.builder()
                .setClaims(claims)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();

        return token;
    }


    public SuccessfulRegistrationResponseDTO registerUser(UserRegistrationDTO userDTO) {
        if (!isValidEmail(userDTO.getEmail()) || !isValidPassword(userDTO.getPassword())) {
            throw new InvalidDataException("Invalid email or password format");
        }

        if (existsUserByEmail(userDTO.getEmail())) {
            throw new DuplicateUserException("Email already registered");
        }

        // Rest of the logic to register a user and generate a token.
        return null;
    }

    private boolean isValidEmail(String email) {
        // Implementa la validación del formato del correo electrónico aquí.
        return true;
    }

    private boolean isValidPassword(String password) {
        // Implementa la validación del formato de la contraseña aquí.
        return true;
    }

    private boolean existsUserByEmail(String email) {
        // Lógica para verificar si existe un usuario con el correo electrónico proporcionado.
        return true;
    }
}
