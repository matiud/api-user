package com.Nisum.apiUser.model.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Phone {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long number;
    private Long cityCode;
    private Long contryCode;
    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;
}
