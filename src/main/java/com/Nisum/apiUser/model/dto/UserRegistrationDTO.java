package com.Nisum.apiUser.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserRegistrationDTO {
    private String name;
    private String email;
    private String password;
    private List<PhoneDTO> phones;
}
