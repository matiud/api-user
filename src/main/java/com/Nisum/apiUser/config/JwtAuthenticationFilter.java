package com.Nisum.apiUser.config;

import com.Nisum.apiUser.exception.TokenNotFoundException;
import com.Nisum.apiUser.model.entity.User;
import com.Nisum.apiUser.service.CustomUserDetailsService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class JwtAuthenticationFilter  extends OncePerRequestFilter {

    @Value("${jwt.secretKey}")
    private String secretKey;

    private CustomUserDetailsService customUserDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String token = extractTokenFromRequest(httpServletRequest);

        UsernamePasswordAuthenticationToken auth = createAuthenticationFromToken(token);
        SecurityContextHolder.getContext().setAuthentication(auth);


        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private String extractTokenFromRequest(HttpServletRequest request) {
        // Extrae el token del encabezado "Authorization"
        String bearerToken = request.getHeader("Authorization");

        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            // Si el encabezado comienza con "Bearer ", extrae el token sin ese prefijo
            return bearerToken.substring(7);
        }

        // Lanza una excepción para indicar que no se encontró un token válido
        throw new TokenNotFoundException("No valid token was found in the request.");
    }

    private UsernamePasswordAuthenticationToken createAuthenticationFromToken(String token) {
        // Aquí deberías decodificar el token y obtener la información del usuario
        Claims claims = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody();

        // Por ejemplo, asumiremos que el token contiene el nombre de usuario
        String email = claims.getSubject();

        // Consulta tu sistema para obtener información adicional del usuario si es necesario
        User userDetails = customUserDetailsService.loadUserByEmail(email);

        List<SimpleGrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority("USER"));
        // Crea un objeto de autenticación con los detalles del usuario y el token
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                userDetails, null, authorities);

        return authenticationToken;
    }


}
