package com.Nisum.apiUser.controller;

import com.Nisum.apiUser.common.ErrorMessage;
import com.Nisum.apiUser.exception.DuplicateUserException;
import com.Nisum.apiUser.exception.InvalidDataException;
import com.Nisum.apiUser.exception.InvalidLoginException;
import com.Nisum.apiUser.model.dto.SuccessfulRegistrationResponseDTO;
import com.Nisum.apiUser.model.dto.UserLoginDTO;
import com.Nisum.apiUser.model.dto.UserRegistrationDTO;
import com.Nisum.apiUser.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserResource {
    @Autowired
    private UserService userService; // Debes tener un servicio para gestionar la lógica de negocio.

    @PostMapping("/regiter")
    public ResponseEntity<?> registerUser(@RequestBody UserRegistrationDTO userDTO) {
        try {
            SuccessfulRegistrationResponseDTO successfulRegistrationResponseDTO = userService.registerUser(userDTO);
            return ResponseEntity.ok(successfulRegistrationResponseDTO);
        } catch (DuplicateUserException e) {
            //El código de estado HTTP 400 se utiliza comúnmente para indicar que la solicitud del cliente no es válida debido a un error en los datos proporcionados por el cliente.
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorMessage("The email already registered"));
        } catch (InvalidDataException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorMessage("Incorrect email or password format"));
        } catch (Exception e) {
            // Manejo de excepciones inesperadas.
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorMessage("Internal server error"));
        }
    }

    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@RequestBody UserLoginDTO userLoginDTO) {
        try {
            // Aquí deberías implementar la lógica de inicio de sesión, verificando el usuario y la contraseña.
            // Si el inicio de sesión es exitoso, genera un token de acceso y retorna la respuesta adecuada.

            // Por ejemplo:
            String token = userService.loginUser(userLoginDTO);
            return ResponseEntity.ok(new SuccessfulRegistrationResponseDTO(token));
        } catch (InvalidLoginException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ErrorMessage("Invalid login credentials"));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorMessage("Internal server error"));
        }
    }
}
